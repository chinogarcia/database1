package mx.univa.database1.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import mx.univa.database1.PersonFormActivity
import mx.univa.database1.R
import mx.univa.database1.models.Person

class PersonAdapter(personList: List<Person>,
                    internal  var context: Context):
    RecyclerView.Adapter<PersonAdapter.PersonViewHolder>() {

    internal var personList: List<Person> = ArrayList()

    init {
        this.personList = personList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonAdapter.PersonViewHolder {
       val view = LayoutInflater.from(context).inflate(
           R.layout.person_item, parent, false)
        return PersonViewHolder(view)

    }

    override fun getItemCount(): Int {
        return personList.size
    }

    override fun onBindViewHolder(holder: PersonAdapter.PersonViewHolder,
                                  position: Int) {
        val person = personList[position]

        holder.name.text = person.name
        holder.phone.text = person.phone
        holder.email.text = person.email

        holder.itemView.setOnClickListener {
            val i = Intent(context, PersonFormActivity::class.java)
            i.putExtra("Mode", "E")
            i.putExtra("Id", person.id)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(i)
        }

    }

    inner class PersonViewHolder(view: View): RecyclerView.ViewHolder(view){
        var name: TextView = view.findViewById(R.id.txv_name)
        var phone: TextView = view.findViewById(R.id.txv_phone)
        var email: TextView = view.findViewById(R.id.txv_email)

    }


}