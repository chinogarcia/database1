package mx.univa.database1.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import mx.univa.database1.models.Person

class DatabaseHandler(context: Context) :
    SQLiteOpenHelper(context, DatabaseHandler.DB_NAME,
        null, DatabaseHandler.DB_VERSION) {

    companion object {
       private val DB_NAME = "AddressBook"
        private val DB_VERSION = 1
        private val TABLE_NAME = "Person"
        private val ID = "Id"
        private val NAME = "Name"
        private val PHONE = "Phone"
        private val EMAIL = "Email"

    }

    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_TABLE = "CREATE TABLE $TABLE_NAME ($ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$NAME TEXT, $PHONE TEXT, $EMAIL TEXT);"
        db.execSQL(CREATE_TABLE)

    }

    override fun onUpgrade(db: SQLiteDatabase,
                           oldVersion: Int, newVersion: Int) {
        val DROP_TABLE = "DROP TABLE IF EXISTS $TABLE_NAME;"
        db.execSQL(DROP_TABLE)
        onCreate(db)

    }

    fun addPerson(person: Person): Boolean{
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(NAME, person.name)
        values.put(PHONE, person.phone)
        values.put(EMAIL, person.email)
        val _success = db.insert(TABLE_NAME,
            null, values)
        db.close()
        return _success > 0
    }

    fun getPerson(_id: Int): Person{
        val p = Person()
        val db = this.readableDatabase
        val query = "SELECT * FROM $TABLE_NAME WHERE $ID = $_id"
        var cursor = db.rawQuery(query, null)
        cursor?.moveToFirst()
        p.id = cursor.getInt(cursor.getColumnIndex(ID))
        p.name = cursor.getString(cursor.getColumnIndex(NAME))
        p.email = cursor.getString(cursor.getColumnIndex(EMAIL))
        p.phone = cursor.getString(cursor.getColumnIndex(PHONE))
        cursor.close()
        return p
    }
    fun listPerson(): ArrayList<Person> {
        val pList = ArrayList<Person>()
        val db = this.readableDatabase
        val query = "SELECT * FROM $TABLE_NAME"
        var cursor = db.rawQuery(query, null)
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                do {
                    val p = Person()
                    p.id = cursor.getInt(cursor.getColumnIndex(ID))
                    p.name = cursor.getString(cursor.getColumnIndex(NAME))
                    p.email = cursor.getString(cursor.getColumnIndex(EMAIL))
                    p.phone = cursor.getString(cursor.getColumnIndex(PHONE))
                    pList.add(p)
                }while (cursor.moveToNext())
            }
        }
        cursor.close()
        return pList
    }

    fun updatePerson(person: Person): Boolean{
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(NAME, person.name)
        values.put(PHONE, person.phone)
        values.put(EMAIL, person.email)
        val _success = db.update(TABLE_NAME, values,
            ID +"=?",
            arrayOf(person.id.toString()))
        db.close()
        return _success > 0
    }
    fun deletePerson(_id: Int): Boolean{
        val db = this.writableDatabase
        val _success = db.delete(TABLE_NAME,
            ID +"=?",
            arrayOf(_id.toString()))
        db.close()
        return _success > 0
    }

    fun deleteAllPerson(): Boolean{
        val db = this.writableDatabase
        val _success = db.delete(TABLE_NAME,
            null,
            null)
        db.close()
        return _success > 0
    }
}