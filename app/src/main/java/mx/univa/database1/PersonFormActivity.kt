package mx.univa.database1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.person_form.*
import mx.univa.database1.db.DatabaseHandler
import mx.univa.database1.models.Person

class PersonFormActivity : AppCompatActivity() {

    var dbHandler: DatabaseHandler? = null
    var isEditMode = false

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.person_form)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initDB()
        initOperations()

    }

    private fun initDB() {
        dbHandler = DatabaseHandler(this)
        btn_delete.visibility = View.INVISIBLE
        if (intent != null && intent.getStringExtra("Mode") == "E") {
            isEditMode = true
            val person: Person = dbHandler!!.getPerson(intent.getIntExtra("Id",0))
            edt_name.setText(person.name)
            edt_phone.setText(person.phone)
            edt_email.setText(person.email)

            btn_delete.visibility = View.VISIBLE
        }
    }

    private fun initOperations() {
        btn_save.setOnClickListener({
            var success: Boolean = false
            if (!isEditMode) {
                val person: Person = Person()
                person.name = edt_name.text.toString()
                person.phone = edt_phone.text.toString()
                person.email = edt_email.text.toString()
                success = dbHandler?.addPerson(person) as Boolean
            } else {
                val personList: Person = Person()
                personList.id = intent.getIntExtra("Id", 0)
                personList.name = edt_name.text.toString()
                personList.phone = edt_phone.text.toString()
                personList.email = edt_email.text.toString()
                success = dbHandler?.updatePerson(personList) as Boolean
            }

            if (success)
                finish()
        })

        btn_delete.setOnClickListener({
            val dialog = AlertDialog.Builder(this).setTitle("Info").setMessage("Click 'YES' Delete the person.")
                .setPositiveButton("YES", { dialog, i ->
                    val success = dbHandler?.deletePerson(intent.getIntExtra("Id", 0)) as Boolean
                    if (success)
                        finish()
                    dialog.dismiss()
                })
                .setNegativeButton("NO", { dialog, i ->
                    dialog.dismiss()
                })
            dialog.show()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}