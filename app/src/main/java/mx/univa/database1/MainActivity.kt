package mx.univa.database1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import mx.univa.database1.adapter.PersonAdapter


import mx.univa.database1.db.DatabaseHandler
import mx.univa.database1.models.Person


class MainActivity : AppCompatActivity() {

    var dbHandler : DatabaseHandler? = null
    var personRecyclerView: RecyclerView? = null
    var personAdapter : PersonAdapter? = null
    var fab : FloatingActionButton? = null
    var pList: List<Person> = ArrayList<Person>()
    var linearLayoutManager: LinearLayoutManager? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        initOperations()

    }

    fun initViews(){
        fab = findViewById(R.id.fab) as FloatingActionButton
        personRecyclerView = findViewById(R.id.rcv_person) as RecyclerView
        personAdapter = PersonAdapter(personList = pList,
            context = applicationContext)
        linearLayoutManager = LinearLayoutManager(applicationContext)
        (personRecyclerView as RecyclerView).layoutManager = linearLayoutManager



    }

    fun initDB(){
        dbHandler = DatabaseHandler(this)
        pList = (dbHandler as DatabaseHandler).listPerson()
        personAdapter = PersonAdapter(personList = pList,
            context = applicationContext)
        (personRecyclerView as RecyclerView).adapter = personAdapter

        Log.i("DATA:", pList.toString())
    }

    fun initOperations() {
        fab?.setOnClickListener { view ->
            val i = Intent(applicationContext, PersonFormActivity::class.java)
            i.putExtra("Mode", "A")
            startActivity(i)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_delete) {
            val dialog = AlertDialog.Builder(this).setTitle("Info").setMessage("Click 'YES' Delete All Person")
                .setPositiveButton("YES", { dialog, i ->
                    dbHandler!!.deleteAllPerson()
                    initDB()
                    dialog.dismiss()
                })
                .setNegativeButton("NO", { dialog, i ->
                    dialog.dismiss()
                })
            dialog.show()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        initDB()
    }

}
