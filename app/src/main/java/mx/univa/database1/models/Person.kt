package mx.univa.database1.models

class Person {
    var id: Int = 0
    var name: String = ""
    var phone: String = ""
    var email: String = ""
}